{
  //Chapter 44
  console.log("Capitulo 44\niteration methods\n");
  grades = [12,13,34,35,5,6,4,3]

  // instead of using a for loop like before we can use the method forEach
  // forEach takes a callback function (basically a function that gets called by another one) which is goin to be called over all the items in the array
  grades.forEach(grade => {
    console.log(grade)
  });
  // we can get access to the index of the element to, below is an example
  grades.forEach((grade, index) => {
    console.log(`${index} - ${grade}`)
  });

  //Chapter 45
  console.log("Capitulo 45\niteration methods multidimentional array\n");
  grades = [[12,13,34,35,5,6,4,3], ["a","b","c","d","e","f","g","h","i","j","k"], [1,2,3,4,5,6,7,8], ["a1","b1","c1","d1","e1","f1","g1","h1"]]

  grades.forEach(row => console.log(row))
  // above we access the row what we need to add is another forEach over the row

  grades.forEach(row => {
    row.forEach(value => console.log(value))
    // break between rows
    console.log("***************************************")
  })

  //Chapter 46
  console.log("Capitulo 46\nlabels\n");
  grades = [[12,13,34,35,5,6,4,3], ["a","b","c","d","e","f","g","h","i","j","k"], [1,2,3,4,5,6,7,8], ["a1","b1","c1","d1","e1","f1","g1","h1"]]

  // what is a label, basically you can names loops and skip to the next iteration of said loop using continue <label>

  outerloop: for(let i = 0; i < grades.length; i++) { // we label the outer loop outerloop
    for(let k = 0; k < grades[i].length; k++) {
      console.log(grades[i][k])
      if (grades[i][k] === "k") { 
        console.log("found the k value")
        continue outerloop // if we found k we don't print the separation between rows because we jump to the label outerloop and go for the next outer loop iteration
        // if we used break we would have printed the separator because the break label only skips the iteration of the inner loop
      }
    }
    console.log("***************************************")
  }

  console.log("------------------------------------------------------")
  
  outerloop2: for(let i = 0; i < grades.length; i++) { // we label the outer loop outerloop
    for(let k = 0; k < grades[i].length; k++) {
      console.log(grades[i][k])
      if (grades[i][k] === "k") { 
        console.log("found the k value")
        break outerloop2 // if we found k we don't print the separation and we don't keep going into the next outerloop iteration, we skip right to the end of the outerloop
      }
    }
    console.log("***************************************")
  }
  // break outerloop2 skips right to out here
  
  //Chapter 47
  console.log("Capitulo 47\nDates!!!\n");

  valentine = new Date();
  // Date() is a constructor, is a special function that returns an instance of something in this case a date which serves to save day, month and year
  // to create a new object froma  constructor we need to use the key word new like above
  // new Date()  creates a date instance with the value of the day, so even though it is called valentine above it will store the actual date in which this script is ran
  console.log(valentine)
  // in the console you should be able to see the date, with the time and timezone in which the user that runs it is
  valentine = new Date(2024, 1, 14); // REMEMBER!!! month is 0 base 11 = December but year and day are not
  // the minimum you need to pass is month and year

  // you can use UnixEpoch to set the date, it means using the milliseconds that passed sin January 1 1970
  unixStart = new Date(0)
  console.log(`Unix start date = ${unixStart}`)

  // you can get the exact number of milliseconds when this code is running with Date.now()
  now = Date.now()
  console.log(`now = ${now}`)

  //Chapter 48
  console.log("Capitulo 48\nDates second class\n");

  // if you pass an invalid date like 32 of December it will turn it into the first of January of the next year
  first = new Date(2024, 11, 32) 
  console.log(`first = ${first}`)

  // to build the date for right now using millisecond (UnixEpoch) you can use Date.now() like below
  now = new Date(Date.now())
  console.log(`now date = ${now}`)

  // a common use for Date.now() is to track how long something took

  start = Date.now()

  x = 0
  for(i = 0; i < 10000000; i++) {
    x += i
  } 
  end = Date.now()

  console.log(`The for took ${end - start} milliseconds`)

  // You can get the difference between two dates
  before = new Date(2024, 11, 12)
  after = new Date(2024, 11, 19)
  console.log(`before = ${before}`)
  console.log(`after = ${after}`)

  // by default substracting between dates returns the amount of milliseconds between one date and the other one
  differenceMilliseconds = after - before;
  console.log(`after - before = ${differenceMilliseconds} milliseconds`)

  // to get the ammount of days between this dates you need to divide the milliseconds by the amount of milliseconds in a day

  millisecondsDay = 1000 * 60 * 60 * 24
  // one second = 1000 milliseconds, a minute = 60 seconds, an hour = 60 minutes and 24 hours = a day

  console.log(`after - before (in days) = ${differenceMilliseconds / millisecondsDay} days`)

  //Chapter 49
  console.log("Capitulo 49\nDate methods\n");

  // first example Date.parse, as Caleb said avoid it if possible because is not guaranteed to work in all browsers
  // with parse you can pass it a string that simbolizes a date and get the Unix Epoch representation
  // remember you can use this value to create a date
  stringDate = new Date(Date.parse("14 December 2024 10:11:12 GMT-0300"))
  console.log(`date from string "14 December 2024 10:11:12 GMT-0300" = ${stringDate}`)

  // if you pass an string that is not expressing a date you will get NaN (Not a Number)
  console.log(`Invalid date string into parse returns ${Date.parse("This is not expressing a date")}`)

  // instead of having to pass through parse first you can pass it directly to the Date constructor like below
  console.log(`new Date("14 Dec 2024 10:11:12 GMT-0300") = ${new Date("14 Dec 2024 10:11:12 GMT-0300")}`)

  // it accepts multiple formats like YYYY-MM-DD GMT-TTTT
  console.log(`new Date("2024-12-14 GMT-0300") = ${new Date("2024-12-14 GMT-0300")}`)

  // you can use libraries for date management but is good to have some knowledge


  // Dates are stored as UTC time and when we print it we get the transformation to our timezone the thing is someone could send us the date in UTC instead of our timezone and in that case we want to save it correctly not with our timezone

  // To do that we use the method Date.UTC which receives a date like new Date but returns the Unix Epoch of that time as if we where in the UTC timezone

  utcMilliseconds = Date.UTC(2024, 1, 14, 10, 10, 10) // the return value is for Valentine's day in UTC
  console.log(`Date.UTC(2024, 1, 14, 10, 10, 10) ${utcMilliseconds}`)
  console.log(`new Date(utcMilliseconds) = ${new Date(utcMilliseconds)}  <- see the difference in the hours, instead of 10 we print 7 (if you are in Uruguay)`)

  // How can I get specific information from a date (this methods are known as getters), for that we have different methods like the ones below
  // getFullYear() returns the full year (YYYY)
  console.log(`valentine.getFullYear() = ${valentine.getFullYear()}`)
  // getMonth() returns the number of the month
  console.log(`valentine.getMonth() = ${valentine.getMonth()} (remember that the month of a date is 0 based)`)
  // getDate() returns the day
  console.log(`valentine.getDate() = ${valentine.getDate()}`)
  // getTimezoneOffset() returns the minutes we are offset compared to UTC (this value is always positive even though you could be x hours ahead or x hours behind)
  console.log(`valentine.getTimezoneOffset() = ${valentine.getTimezoneOffset()} minutes = ${valentine.getTimezoneOffset() / 60} hours`)


  // what if I want to change the information of the date? you can use the setter methods provided by date
  // setFullYear(YYYY), with this you can set the year of an already created date it returns the Unix Epoch but alters the date
  console.log(`valentine.setFullYear(2025) = ${valentine.setFullYear(2025)}`)
  console.log(`valentine year modified = ${valentine}`)
  // setMonth(MM), with this you can set the month of an already created date it returns the Unix Epoch but alters the date
  console.log(`valentine.setMonth(2) = ${valentine.setMonth(2)}`)
  console.log(`valentine month modified = ${valentine}`)
  // setDate(DD), with this you can set the day of an already created date it returns the Unix Epoch but alters the date
  console.log(`valentine.setDate(15) = ${valentine.setDate(15)}`)
  console.log(`valentine day modified = ${valentine}`)



  console.log("you can check https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Date for more methods")

} 

