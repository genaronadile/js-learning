{
    //Capitulo 30
    console.log("Capitulo 30\nloop\n");

    //loop (una boludes pero me parecio interesante)

    /*
        Los loops estan basados en tres principios:
        I nicilization (seteas una variable)
        C ondition (chequeas una condition)
        U pdate (actualizas la variable)

        ejemplo a continuacion para identificar estos principios
    */

    let i = 0;//Initialization

    while (i < 10) {//condition

        //codigo random

        i++;//Update
    }

    //ademas del while esta el for y el do while
    /*
     While cuando no sabes cuantas veces se va a ejecutar algo
     For cuando sabes que se la cantidad de veces que se va a ejecutar algo
     Do while cuando no sabes cuantas veces se va a ejecutar algo, pero sabes que por
              lo menos una vez si se debe ejecutar
     */


    //Me parecio copada la vision de los loops (no se me hizo acordar a mi ni�ez de prog 1 jajaj)

    //--------------------------------------------------------------------------------------------------------

    //Capitulo 31
    console.log("\nCapitulo 31\nSintaxis loops \n");
    // Sintaxis loops 

    /*
     principios de los loops:(recordatorio) 
     I nitialization
     C condition
     U pdate
     */

    //while
    i = 0; // Initialization
    while (i < 3) {//Condition
        console.log("while", i);
        i++;//Update
    }
    console.log("\n");

    //do while
    //ejecuta por lo menos una vez lo que esta en el do

    i=0//initialization
    do {
        console.log("do while", i);
        i++;//Update
    } while (i < 3);//Condition
    //CUIDADO no olvida el ;
    console.log("\n");

    //for
    for (j = 0; j < 3;j++) {
        console.log("for", j);
    }

    //--------------------------------------------------------------------------------------------------------

    //Capitulo 32
    console.log("\nCapitulo 32\n");
    // Ejemplos de loops
    //no me parecen muy interesantes

    //--------------------------------------------------------------------------------------------------

    //Capitulo 33
    console.log("\nCapitulo 33\nContinue and break\n");
    //Continue and break

    let miString = "search this string baby c";//I don't know it was like this in the video hahaha
    let charABuscar = "c";
    for (i = 0; i < miString.length; i++) {
        if (miString[i] === charABuscar) {
            console.log(charABuscar + " se encuentra en la posicion " + i);
        }
    }
    //en este for se recorre todo el string encontrando todas lass ocurrencias del char
    //en el string. Si queres solo encontrar la primera el siguiente for es el correcto

    for (i = 0; i < miString.length; i++) {
        if (miString[i] === charABuscar) {
            console.log(charABuscar + " se encuentra en la posicion " + i);
            break;
        }
    }
    //break detiene la funcion en la cual se encuentra ejecutando, en el caso anterior
    //el for

    //continue permite seguir en la misma funcion pero saltar hasta la siguiente iteracion

    //ejemplo imprimir solo las consonantes
    for (i = 0; i < miString.length; i++) {
        if (miString[i] === "a" || miString[i] === "e" || miString[i] === "i" ||
            miString[i] === "o" || miString[i] === "u") {
            continue;//se salta lo que esta abajo del if
        }
        console.log(miString[i]);
    }
    //se pod�a hacer con un else pero bueno es un ejemplo

    //-----------------------------------------------------------------------------------------------

    //Capitulo 34
    console.log("\nCapitulo 34\nnested loops\n");
    //nested loops 

    //creamos una piramide en la pagina
    let d = document.getElementById("piramide");
    for (i = 0; i < 10; i++) {
        for (k = i; k >= 0; k--) {
            d.append(k +" ");//a piramide le agregamos el numero k y un espacio
        }
        var br = document.createElement("br");//creamos un salto de linea
        d.appendChild(br);
    }

    //-----------------------------------------------------------------------------------------------

    //Capitulo 35
    console.log("\nCapitulo 35\nintro arreglos\n");
    //intro arreglos (cosas obvias pero bueno ya que estamos)

    //array podes pensar como si fuera una coleccion de cosas (por lo general suelen ser del mismo tipo)
    //sintaxis : let variable = [valor, valor, ....];
    //son indexados en 0, o sea si queres el primer elemento haces arreglo[0]
    //arreglo.length es una propiedad la cual devuelve la cantidad de elementos en un array

    /*
     LO QUE SI ES RARO jaja

     Si te pasas del largo de un array no te va a dar error solo te va a mostrar undefined
     Si haces arreglo.length = Numero mas grande, estas alargando el arreglo y los elementos que
     se agregan tienen el valor undefined
     Si haces arreglo[numero mayor que el (largo-1)] = valor, seteas ese valor y alargas el arreglo
     dejando todos los nuevos elementos en las posiciones entre largo y ese numero -1 como undefined

     OJO CON ESTO PORQUE NO ES C (Our savior jaja)
     ejemplos a continuacion
     */

    let arreglo = ["Hola.", "Que tal?"];
    console.log(arreglo);
    console.log("indice 3 del arreglo vale", arreglo[3]);//muestra undefined
    arreglo.length = 3;//sumarle al largo uno mas, agrega un elemento con udefined
    console.log(arreglo);
    arreglo[4] = "?";//agrega en el indice cuatro el ?, y en la pos 3 y 2 queda undefined
    console.log(arreglo);
    arreglo[3] = "tal";
    arreglo[2] = "Que";
    console.log(arreglo);


    //-----------------------------------------------------------------------------------------------

    //Capitulo 36
    console.log("\nCapitulo 36\nintro a arreglos multidimensionales\n");
    //intro a arreglos multidimensionales

    //arreglos de arreglos es eso, jemplos a continuacion (no son del video)

    let notas = [
        [12, 3, 8],
        [11, 10, 12],
        [7, 5, 4],
        [12, 12, 12]
    ];
    //si quiero un elemento en particular uso [][]
    console.log(notas[3][1]);
    //el array entero
    console.log(notas[0]);

    //-----------------------------------------------------------------------------------------------

    //Capitulo 37
    console.log("\nCapitulo 37\narreglos\n");
    //arreglos
    //los arreglos al contrario de en otros lenguajes, pueden tener distintos tipos de valores
    arreglo = [123, "Holaaa", function () { console.log("re loco") }];
    console.log(arreglo);

    //ademas pueden tener funciones (tipo programacion funcional)
    //para utilizar las funciones en un array hacemos lo siguiente
    arreglo[2]();//vas a la posicion en el array con [] y le agregamos () que realiza el llamado

    //si deseas agregar elementos a un array, simplemente agregas un indice y le asignas un valor 
    //a esa posicion del array
    //en este ejemplo el mayor indice es 2 para agregar un elemento podemos hacer
    
    arreglo[3] = "nuevo valor";
    console.log(arreglo);
    //ademas esta el metodo array.push(valor) se usa mas adelante


    //no tenes porque usar el length +1, podes agregar valores en cualquier indice, lo unico es que
    //el arreglo te pone los valores entre medio como undefined. El length siempre es el ultimo
    //index +1, en este caso si pongo algo en arreglo[10] el largo sera 11
    arreglo[10] = "el largo ahora es";
    console.log(arreglo[10], arreglo.length);
    console.log(arreglo);

    //lo que podes hacer para agregar lugares sin insertar nada es cambiar el valor de length
    arreglo.length = 20;
    console.log(arreglo);

    //ademas podes haciendo lo mismo pero reduciendo el largo, ccortar el arreglo

    arreglo.length = 3;
    console.log(arreglo);

    //-----------------------------------------------------------------------------------------------

    //Capitulo 38
    console.log("\nCapitulo 38\n");
    //iterar sobre arreglos, busquedas y aceptar un array de input

    //estoy perezoso no quiero hacer una busqueda ya se sabe como es
    //idem buscar le maximo y minimo 

    //-----------------------------------------------------------------------------------------------

    //Capitulo 39
    console.log("\nCapitulo 39\n");
    //promedio de un arreglo 
    //no voy a hacerlo porque se me cae el alma, ademas lo unico importante es tener en cuenta los undeffined

    //-----------------------------------------------------------------------------------------------

    //Capitulo 40
    console.log("\nCapitulo 40\nrellenar un array con input del usuario\n");
    //rellenar un array con input del usuario

    arreglo = [];

    while (true) {
        let input = prompt("agrega un numero al array ('q' para terminar)");
        if (input === "q" || input===null)//null por si el usuario le da a cancel
            break;
        arreglo.push(Number(input));//con esto podes ir agregando valores a un array
    }
    console.log(arreglo);

    //-----------------------------------------------------------------------------------------------

    //Capitulo 41
    console.log("\nCapitulo 41\narray methods\n");
    //array methods

    arreglo = [1,2,3,4,5,6,7,8,9];
    console.log(arreglo);
    //utilizado previamente arreglo.push(valor), modifica el arreglo agregando valor al final y 
    //devuelve el largo con el cual queda el arreglo
    console.log(arreglo.push("hice push"));
    console.log(arreglo);

    //arreglo.pop(), retira el ultimo valor del arreglo y lo devuelve
    console.log(arreglo.pop());
    console.log(arreglo);

    //arreglo.unshift(valor), como el push pero al principio
    console.log(arreglo.unshift("hice unshift"));
    console.log(arreglo);

    //arreglo.shift(), como pop pero al principio
    console.log(arreglo.shift());
    console.log(arreglo);

    //arreglo.splice(cosas), nos permite trabajar en la mitad del array quitando, sustituyendo 
    //y agregando valores dependiendo de el cosas. Devuelve un array con los valores borrados siempre

    //borrar valores: arreglo.splice(indice de inicio, cuantos elementos borrar) 
    console.log("arreglo.splice(3,3) devuelve "+arreglo.splice(3, 3));
    console.log("asi quedo el arreglo " + arreglo);

    //agregar valores: arreglo.splice(inicio,0,valor,valor,valor,....). El 0 es porque no queremos
    //borrar nada
    console.log(`arreglo.splice(3,0,4,5,6) devuelve ${arreglo.splice(3, 0, 4, 5, 6)}`);
    console.log(`asi quedo el arreglo ${arreglo}`);

    //para sustituir arreglo.splice(inicio,cantElemsSust, valor1,valor2,..,valorCantElemsSust)
    console.log("arreglo.splice(3,3,21,12,34) devuelve " + arreglo.splice(3, 3, 21, 12, 34));
    console.log(`asi quedo el arreglo ${arreglo}`);

    //-----------------------------------------------------------------------------------------------

    //Capitulo 42
    console.log("\nCapitulo 42\narray methods 2\n");
    grades = [1, 0, 7, 5, 3, 4, 10, 11, 14, 0, 0, 12, 21, 26, 23];

    // sort
    console.log(`sort() by default sorts stuff not numerically but alphabetically ${grades.sort()}`)
    
    console.log(`sort((a,b) => a - b)) sorts by the value of the number ${grades.sort((a,b) => a - b)}`)
    // if the return of the callback function used in sort is negative the numbers or values get flipped, if not they stay the way they are
    // it modifies the array and returns the reference to the same array

    // reverse
    // as sort it modifies the array and returns the reference to the array again
    // as the name i
    console.log(`reverse() reverses the array ${grades.reverse()}`)
    
    //fill
    //Fills a piece of the array with an specified value, along with the value you can specify with start and end the piece of the array you want to fill (end is not changed)
    console.log(`fill(-1, 0, grades.length) fills the entire array with -1 ${grades.fill(-1, 0, grades.length)}`)

    //copyWithin
    // Allows you to copy a piece of the array from a point in that array onwards
    example_copy_within = [0, 1, 2, 3, 4, 5, 6, 7]
    console.log(`copyWithin(0, 5, 6) copies 5 over the number 0 -> ${example_copy_within.copyWithin(0, 5, 6)}`)

    console.log(`copyWithin(0, 5) copies 5,6,7 over the numbers 5,1,2 -> ${example_copy_within.copyWithin(0, 5)}`)
    // since we didn't specify an end it goes from the position 5 till the end of the array

    //-----------------------------------------------------------------------------------------------

    //Capitulo 43
    console.log("\nCapitulo 43\narray methods 3\n");
    grades = [20, 22, 39]
    gradesB = [1, 2, 3, 4]
    
    //concat
    console.log(`grades.concat(gradesB) concatenates the arrays grades and gradesB in that order and returns it (IT DOESN'T CHANGE THE ARRAY) -> ${grades.concat(gradesB)}`)
    allGrades = grades.concat(gradesB)

    // if you tried to use + instead of concat you will get an string with all items in the arrays separated by commas

    // includes
    console.log(`allGrades.includes(20) returns true if the value 20 is in the array allGrades -> ${allGrades.includes(20)}`)

    // indexOf
    console.log(`allGrades.indexOf(20) returns the index where the value 20 is in, if we get -1 the element is not there -> ${allGrades.indexOf(20)}`)

    // join
    console.log(`allGrades.join() returns the elements of the array in a string sepparated by commas -> ${allGrades.join()}`)
    console.log(`allGrades.join("+") instead of commas it uses pluses -> ${allGrades.join("+")}`)

    // slice
    console.log(`allGrades.slice(3,5) returns a shallow copy of the positions 3 and 4 of the array allGrades -> ${allGrades.slice(3,5)}`)
    // if 5 is not specified it goes till the end of the array
    // shallowcopy means it doesn't create copies of the items inside the items of the array, look example below
    shallowCopyExample = [{a: 1, b: 2}]
    console.log(`shallowCopyExample -> ${shallowCopyExample}`)
    shallowCopyExampleSlice = shallowCopyExample.slice()
    console.log(`shallowCopyExampleSlice = shallowCopyExample.slice() -> ${shallowCopyExampleSlice}`)

    shallowCopyExampleSlice[0].a = 99
    console.log(`shallowCopyExampleSlice[0].a = 99, shallowCopyExample -> ${JSON.stringify(shallowCopyExample)}, shallowCopyExampleSlice -> ${JSON.stringify(shallowCopyExampleSlice)}`)
    // note the a changes to 99 in "both dictionaries", that happens because slice doesn't make new dictionaries it only creates a new reference to the dictionaries inside the array, that is a shallow copy
} 

