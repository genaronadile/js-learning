(function(){//iife imediatley invoked function expression
            // evita que queden variables globales
            //portege las variables mientras uses var antes de declararlas
})();

{
    let a = 12;//esta variable solo esta entre los {}
    const x = 20;//esta es una constante que esta solo entre los {}
    //Caleb Curry recomienda esto 

    //primitive types
    /*
        String,number,boolean,null,undefined
        si creas una variable y no la inicializas el valor de esta es undefined
        null  se usa cuando queres marcar que la variable no tiene un valor definido
        buena practica dejar que undefined lo use solo la maquina
    */
    
    //Objects
    /*
        Todo lo que no son primitivos
    */
    //Ejemplo
    let person = {
        name: "Genaro",
        age: "23",
        favFood: "milanesa",
        work: "facultad",
        fun: function(){
            console.log("hola que hace, soy "+person.name);
        }
    }
    //funciones dentro de objetos (fun) son llamadas metodos
    person.fun();

    let now = new Date();

    let arr = [10,20,30,40,50];

    /*primitivos son inmutable reasignar valores a una variable implica que el
      valor anterior es descartado definitivamente.
      Los tipos primitivos aunque no tienen metodos como los objetos tienen
      se pueden utilizar propiedades, metodos de estos tipos, esto se debe a que
      debajo se lo envuelve el tipo en su contraparte objeto, se realiza el metodo
      y se lo convierte nuevamente a el tipo primitivo.
    */
    let str = "Hola1"
    let strObj = new String ("Hola2")
    console.log(str+" -- "+strObj);
    console.log(typeof (str)+" -- "+typeof (strObj));


    //-------------------------------------------------------------------------------

    //Cap 13 datatype Numbers

    /*el almacenamiento de decimales es realizado con doble precision
    tenemos Nan, inf y -inf.
    La verdad no muy productivo el cap
    */
    
    //----------------------------------------------------------------------------------

    //Cap 14 y 15 Operadores, asignacion, incrementos y decrementos
    /*
     % = mod (pero funciona con racionales, podes hacer 10 % 4.25 = 1.5)
     
     la precedencia de operadores aritmeticos es igual a la de la escuela
     + - separan, con los )( podes cambiar esto
     Caleb recomienda usar )( para que quede claro como se evalua, aunque 
     no sea necesario

     Funciona
     variable++ y variable--
     ++variable y --variable
     Los dos renglones hacen lo mismo pero se comportan distinto a
     continuacion codigo mostrando la diferencia
     */
    let parte14 = 10;
    let parte14despues = parte14++;
    console.log("parte14:", parte14);
    console.log("parte14despues:", parte14despues);
    parte14--;
    let parte14antes = ++parte14;
    console.log("parte14antes:", parte14antes);
    /*
     Con los ++ despues entonces el codigo se ejecuta primero con el valor
     de la variable sin aumentar, y si el ++ esta antes, entonces se 
     ejecuta con el valor aumentado.
     */



    /*
     Tambien funciona:
     +=
     -=
     /=
     %=
     */

    //------------------------------------------------------------------------------------

    //Cap 16 Number Methods

    /*
     Estos metodos se acceden utilizando Number.NombreDelMetodo
     Pueden verse como metodos estaticos de la clase
     */

    //Number.parseInt
    //transforma stings a numeros, busca un numero dentro del string, y lo trunca

    let parseDeInt = Number.parseInt("17.1717171717 es mi numero favorito"); 
    console.log(parseDeInt);
    //Number.parseFloat
    // idem parseInt pero no truncaa el n�mero

    let parseDeFloat = Number.parseFloat(" \n          17.1717171717 es mi numero favorito");
    console.log(parseDeFloat);

    /* 
     El numero en los dos casos debe estar al principio no pueden haber
     simbolos adelante solo espacios, enters y esas cosas
    */


}