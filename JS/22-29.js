{
    //Capitulo 22
    console.log("Capitulo 22\n");

    //Functions and objects

    //to declare an object you use {} and all that is between those brackets is part of the object
    let position = {
        x: 20,
        y: 60,
        print: function(){//as� se declara un metodo de un objeto
            console.log(`x =${this.x} y=${this.y}`);
        },
        otroObjeto: {prop:"sabelo"}//objeto adentro de objeto
    }
    //la asignacion de objetos es como punteros o sea x = obj e y=x hace que y apunte al mismo lugar que x

    let myPosition = position
    myPosition.x = 235;
    console.log("position =", position, "\t myPosition =", myPosition);
    //Cambiaron position y myPosition porque en realidad son punteros

    position.print();//as� llamas a un metodo de un objeto

    console.log(position.otroObjeto.prop);

    //-------------------------------------------------------------------------------------------------------

    //Capitulo 23
    console.log("\nCapitulo 23\n");
    //if, else, else if

    //if(){} estructura del if (Como toda la vida, las otras tambien)
    //Importante === chequea tipos el == solo valores, a continuacion ejemplos
    console.log("5==\"5\"", 5 == "5");
    console.log("5===\"5\"", 5 === "5");
    //recomendado usar ===

    //--------------------------------------------------------------------------------------------
    //el 24 es mas una charla que otra cosa
    //Capitulo 25
    console.log("\nCapitulo 24 y 25\n");

    //operadores de comparacion
    //! = not, el distinto es !== (el que tiene en cuenta el tipo, el otro es solo sacar un =)
    //<=,<,>,>= no tienen en cuenta el tipo (OJO)

    //------------------------------------------------------------------------------------------------------------

    //Capitulo 26
    console.log("\nCapitulo 26\n");
    //operadores logicos

    //or ||, and &&, ! not
    //recomienda no mezclar ors y ands en una misma condicion

    //------------------------------------------------------------------------------------------------------------

    // Capitulo 27
    console.log("\nCapitulo 27\n");
    //Switch statement
    /*
        switch(variable){
            case valor:
                codigo1
                break;
            case otro valor:
                codigo2
                break;
            default://esto es para todos los casos que no entren en los anteriores
                codigo3
                break;
        }

    break esta para que solo se ejecute ese codigo, si no estuviera abaajo de codigo 1
    entonces se ejecutaria el 1 el 2.(ejemplo a continuacion)
    recomendado usarlo siempre
    Si dos casos implican el mismo codigo podes hacer asi:
        case valor:
        case Otro valor:
            codigoComun
            break;
    es un caso en el que podes no poner un break para el case valor
    */

    let variable = 1;
    switch (variable) {
        case 1:
            console.log("sin el");
        case 2:
            console.log("break");
        default:
            console.log("!!!!")
    }

    switch (variable) {
        case 1:
            console.log("con el");
            break;
        case 2:
            console.log("break");
            break;
        default:
            console.log("!!!!")
            break;
    }

    //------------------------------------------------------------------------------------------------------------

    // Capitulo 28
    console.log("\nCapitulo 28\n");
    //single line if statement

    // el if al igual que en c++ si tenes un if de una sola linea no tenes porque usar {}
    //recomienda que escribas todo en una linea, o sea if(condicion) linea de codigo
    if (variable === 1) console.log("Un if de una linea sola wow");

    if (variable === 2) console.log("esto no se ejecuta lo siguiente si");
    console.log("awesome");//esta linea se ejecuta siempre, no esta atada al if anterior

    //------------------------------------------------------------------------------------------------------------

    // Capitulo 29
    console.log("\nCapitulo 29\n");
    //operadores ternarios

    //toman tres valores, sintaxis variable = (condicion)?valor si es true:valor si es falso
    let num = prompt("ingresa un numero?");
    let points = (num == 1) ? 10 : 0;
    console.log("puntos = ", points);

    //o podes poner sentencias, sintaxis (condicion)?sentencia si es true:sentencia si es falso
    num = prompt("ingresa un segundo numero?");
    (num == 1) ? console.log("puntos = 10") : console.log("puntos = 0");

    

} 
