{
    console.log("Capitulo 17");
    //Capitulo 17
    let input = prompt("Put in a number");
    //parsear string a numeros en distintas bases
    console.log("Decimal  ", input);
    console.log("Binary   ", Number.parseInt(input, 2));//this way you cast it to binary
    console.log("Octal    ", Number.parseInt(input, 8));//octadecimal
    console.log("Hex      ", Number.parseInt(input, 16));//hexadecimal

    //obtener representasion de los numeros en otras bases
    input = Number.parseInt(input);
    console.log(input + " in Binary: " + input.toString(2));
    console.log(input + " in Octal : " + input.toString(8));
    console.log(input + " in Hez   : " + input.toString(16));

    //-----------------------------------------------------------------------------------------

    //Capitulo 18
    console.log("\nCapitulo 18\n");
    //Number instance functions y Math object

    /*
     Number es un constructor, �en javascript las funciones son objetos?�es como funcional?
     Las funciones de este capitulo son los que se llaman sobre una variable number
     */

    //Math object ejemplos

    
    console.log("abs de -36 es :", Math.abs(-36));
    console.log("ceiling de 0.00001 es :", Math.ceil(0.00001));
    console.log("floor de 0.00001 es :", Math.floor(0.00001));
    console.log("3 elevado a la 2 es :", Math.pow(3, 2));
    console.log("4.9 redondeado es :", Math.round(4.9));
    console.log("-infinito es positivo? ", Math.sign(-Infinity));
    console.log("infinito es positivo? ", Math.sign(Infinity));
    console.log("4.95 truncado es :", Math.trunc(4.95));

    //---------------------------------------------------------------------------------------------------

    //Capitulo 19
    console.log("\nCapitulo 19\n");
    //Strings (Yaaay)
    let miNombre = "Genaro";
    /*
      los string como en todos lados (creo jaja) es un arreglo de chars, pensando de esta
      forma podes acceder a caracteres individuales como en un arreglo.
      Java script empieza los arreglos en la posicion 0
    */
    console.log("la primera letra de Genaro es:", miNombre[0]);
    //IMPORTANTE: Si te vas del arreglo javascript no da error, te devuelve undefined
    console.log("Si te vas del arreglo javascript no da error, te devuelve undefined");
    console.log(miNombre[1500]);

    //Templetize strings, la idea es que le das una estructura y el resultado depende de una
    //variable o algo as�, igual a continuaci�n un ejemplo

    console.log("re loco, " + miNombre + " sabe");//esta forma es muy extensa implica abrir y cerrar comillas

    console.log(`re loco, ${miNombre} sabe`);//esta no implica tanto, caleb la recomienda (Caleb sabe)

    //Tener en cuenta que los strings tienen metodos asosiados al igual que los numbers (look for them lazy sloth)

    //---------------------------------------------------------------------------------------------------

    //Capitulo 20
    console.log("\nCapitulo 20\n");

    //String Methods
    let inicio = "para concatenar usa "
    console.log(inicio.concat(".concat" + " o +"));

    //para buscar strings en un string usas includes
    let inclu = "esto es un string normal";
    console.log("inclu = " + inclu);
    console.log("inclu tiene string? ", inclu.includes("string"));
    console.log("inclu tiene number? ", inclu.includes("number"));

    //tambien podes buscar un string desde cierto indice en adelante

    console.log("inclu tiene string del indice 17 en adelante? ", inclu.includes("string",17));

    //si queres saber la posicion donde empieza un string usas indexOf (desde el principio)
    console.log("string en inclu empieza en :", inclu.indexOf("string"));
    /*
      te da la primera ocurencia.
      Al igual que con includes podes aclararle el indice desde donde queres que arranque
    */

    //Si queres la primera aparicion desde el final usas lastIndexOf

    console.log("la ultima aparicion de la o en inclu esta en: ", inclu.lastIndexOf("o"));

    //---------------------------------------------------------------------------------------------------------
    //Capitulo 21
    console.log("\nCapitulo 21\n");

    //substring primer argumento punto de inicio, segundo final (no incluye a ese indice el resultado)
    console.log(inclu.substring(0, 4), "= inclu.substring(0,4)");

    //substr hace lo mismo a substring solo que el segundo agumento indica la cantidad de chars desde el inicio a incluir
    console.log(inclu.substr(0, 4), "= inclu.substr(0,4)");

    //existe slice pero a buscar en la web si lo necesitas

    // toLowerCase y toUpperCase 

    //trim elimina los espacios (\n,\t, etc) del principio y final
    let exampleTrim = "      \n\t\n Hola que tal como te va?      \n\n\t";
    console.log(`exampleTrim=${exampleTrim}`);
    console.log(`exampleTrim.trim()=${exampleTrim.trim()}`);
    
    //tambien existe trimRight(=trimStart) y trimLeft(=trimEnd)

    //repeat que repite la cantidad de veces indicada el string

    //split toma un separador y separa el string en un array 
    console.log("separar por \" \":\n", inclu.split(" "));



} 
